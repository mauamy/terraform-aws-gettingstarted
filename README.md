# Terraform/AWS Getting Started

## Installation and Credentials Setup

The first step for using **Terraform** and AWS is to install the required tools.

To install terraform, simply go to the [Terraform Download Page](https://www.terraform.io/downloads.html)
and download the binary for your system.

In addition to *terraform* you need to install *awscli*. *awscli* is provided as Python-PIP package, so
you can simply install it via `pip install awscli --user`.

### Setup bash completion

## Setup AWS Credentials
Now you need to setup your AWS-CLI credentials, which are used for *awscli* and *terraform*.
First login to your [AWS Console](https://console.aws.amazon.com) Account and click on **My Security Credentials**.
On this page simply go to **Access keys for CLI, SDK, & API access** and click on *Create access key*. This will 
generate your AWS-CLI credentials in a CSV file you have to download.

Now you can actually setup your credentials for *awscli*:

```bash
$ aws configure
AWS Access Key ID [None]: <YOUR_ACCESS_KEY>
AWS Secret Access Key [None]: <YOUR_SECRET_ACCESS_KEY>
Default region name [None]: eu-central-1
Default output format [None]: json
```     

These informations will be stored under *~/.aws/credentials* and *~/.aws/config*.

All regions can be found [here](https://docs.aws.amazon.com/de_de/general/latest/gr/rande.html).


Now that the aws credentials are set up, we can start using terraform.


# Terraform Example
In this terraform example we will setup a VPC including a EC2 instance that is accessible via
SSH from the outside.

## Setup the provider informations
Therefor we start with creating a terraform file **olymp.tf** and setup the provider.
```hcl-terraform
provider "aws" {
  access_key = "ACCESS_KEY_HERE"
  secret_key = "SECRET_KEY_HERE"
  region     = "eu-central-1"
}
```
**Note:** If you leave out the *access_key* and *secret_key* terraform will use the credentials
stored in *~/.aws/credentials*.

Now we can run the command `terraform init`. Terraform will now download and setup the required
provider binaries for us. As soon this command finishes, terraform should be ready to use.

## Creating a AWS VPC
In general we define a **terraform resource** by creating a **resource**-block:
```hcl-terraform
resource "RESOURCE_TYPE" "RESOURCE_NAME" {
  ...
}
```
The *RESOURCE_TYPE* defines which type of resource we want to create. Each provider provides its 
own set of resource types. These can be found in the corresponding [documentation](https://www.terraform.io/docs/providers/index.html).

The *RESOURCE_NAME* is the name for the resource that can be used in context of terraform to reference
this resource, as we see in a later example.

To create a AWS VPC we now add the following to **Olymp.tf**:

```hcl-terraform
resource "aws_vpc" "olymp" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "olymp"
  }
}
```

This will create a VPC with the name "olymp" and the adds the IPv4 Range *10.0.0.0/16* to it.
The tag `Name = "olymp"` is used to set the name for this resource in AWS. So this tag has nothing to do
with any terraform references we see in later examples.

Further argument references for AWS VPCs can be found [here](https://www.terraform.io/docs/providers/aws/r/vpc.html).

## Plan and Apply
Now that we have defined a resource we could create it in AWS.
But before we actually apply our resources, lets first check which steps will be performed.
Therefor we run `terraform plan`. Terraform will now process all ***.tf**-files, check for any syntax errors, create an
execution plan and print it on standard output.
If we are happy with the generated plan, we can run `terraform apply` and the resources will be created.

## Creating a Subnet
Before we can crete a EC2-instance in our VPC we first need to create a corresponding subnet
in our VPC-network (*10.0.0.0/16*):

```hcl-terraform
resource "aws_subnet" "styx" {
  vpc_id = "${aws_vpc.olymp.id}"
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "styx"
  }
}
```
Here we create a resource of type *aws_subnet* and name it *styx*. The subnet has the IPv4-range
*10.0.1.0/24*. The line `vpc_id = "${aws_vpc.olymp.id}"` assigns this subnet with the previously defines
VPC *olymp*. Here aws_vpc.**olymp**.id references the *resource_name* of `resource "aws_vpc" "olymp" { ... }`.

Further argument references for AWS subnets can be found [here](https://www.terraform.io/docs/providers/aws/r/subnet.html).

## Creating a EC2-instance
Now that we have a VPC with a subnet, we can finally create a EC2-instance:

```hcl-terraform
resource "aws_instance" "olymp-ubuntu" {
  ami           = "ami-0bdf93799014acdc4"
  instance_type = "t2.micro"

  subnet_id = "${aws_subnet.styx.id}"

  associate_public_ip_address = true

  key_name = "olymp-key"

  tags = {
    Name = "olymp-ubuntu"
  }
}
```

*AMI* stands for *Amazon Machine Images* and defines which kind of Operating-System should
run on our EC2-instance. The parameter *instance_type* defines the provided hardware-power.
Details of all instance type can be found [here](https://aws.amazon.com/de/ec2/instance-types/).
With the line `subnet_id = "${aws_subnet.styx.id}"` we state that the EC2-instance shell be deployed 
in our previously defined subnet *styx*. The parameter *associate_public_ip_address* ensures that out EC2-instance
will get a public ip address. The *key-name* is the name of the RSA-key-pair that can be used
to remotely access this EC2-instance. If you do not already have created a key-pair, you simply need to go to your
[AWS Console](https://aws.amazon.com/de/console/) into the EC2 section. Under *Network & Security* click on
*Key Pairs* and create a new key-pair with a name of your choice. After creating the key-pair you must download it.

 **Attention: This is the only time you can access the RSA private key, so download it and keep it somewhere save.** 

Remember to change the name of the key in your terraform-file, too. 

Further argument references for AWS instances can be found [here](https://www.terraform.io/docs/providers/aws/r/instance.html#block-devices).

## Creating a Security Group
So now that we have a VPC, a subnet and a EC2-instance in this subnet, we want to allow SSH access from the outside.
Therefor we need to create a so called *security group* by adding this before our EC2-instance:

```hcl-terraform
resource "aws_security_group" "charon" {
  name = "charon"
  description = "Allow SSH inbound"
  vpc_id = "${aws_vpc.olymp.id}"

  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22

    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "charon"
  }
}
```
By setting the *vpc_id* we assigning this security group to our VPC.
The *ingress*-block defines the ingoing rules while the *egress*-block defines the outgoing rules.
The *cidr_block* parameter defines the allowed source/destination addresses.

Further argument references for AWS security groups can be found [here](https://www.terraform.io/docs/providers/aws/r/security_group.html).

Finally we need to tell our EC2-instance to use this security group by adding the following line
to our EC2-instance:
```hcl-terraform
resource "aws_instance" "olymp-ubuntu" {
  ...
  vpc_security_group_ids = ["${aws_security_group.charon.id}"]
  ...
}
```

Now that all is set up we finally can apply our resources.
First we run `terraform plan` to see which steps will be performed.
Now you can run `terraform apply` to apply the resources. When this process has finished,go
to your AWS Console and check if the resources have been created successfully. Also check the public IP-address/domain-name of
the new EC2-instance. 

You can now try to connect to this machine via SSH:
```bash
$ ssh -i "olymp-key.pem" ubuntu@<IP/domain-name>
```
You will recognize that this is still **not** working. 

## Creating Gateways and Routing Tables
If you check the routing table of your newly created subnet in the AWS Console, you can see
that there is no internet gateway defined. So we need to create this internet gateway, add it to out
VPC, create a routing table and associate with our subnet:

```hcl-terraform
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.olymp.id}"
}

resource "aws_route_table" "rt" {
  vpc_id = "${aws_vpc.olymp.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }
}

resource "aws_route_table_association" "rta" {
  route_table_id = "${aws_route_table.rt.id}"
  subnet_id = "${aws_subnet.styx.id}"
}
```
Further argument references for AWS internet gateways can be found [here](https://www.terraform.io/docs/providers/aws/r/internet_gateway.html).

Further argument references for AWS route tables can be found [here](https://www.terraform.io/docs/providers/aws/r/route_table.html).

Further argument references for AWS route table association can be found [here](https://www.terraform.io/docs/providers/aws/r/route_table_association.html).

Now we need to add an additional line to our EC2-instance:
```hcl-terraform
resource "aws_instance" "olymp-ubuntu" {
  ...
  depends_on = ["aws_internet_gateway.gw"]
  ...
}
```
This line denotes, that the EC2-instance depends on the internet gateway.

## Final Apply
Now that we have set everything up correctly, we run `terraform plan` and `terraform apply` again.
After these changes have been applied successfully, we can now successfully access our EC2-instance via SSH:
```bash
$ ssh -i "olymp-key.pem" ubuntu@<IP/domain-name>
...
ubuntu@ip-10-0-1-xxx:~$
```

# References
* [Terraform Getting Started](https://learn.hashicorp.com/terraform/getting-started/install)
* [Terraform AWS Provider Documentation](https://www.terraform.io/docs/providers/aws/index.html)
* [AWS-CLI Userguide](https://docs.aws.amazon.com/de_de/cli/latest/userguide/cli-chap-welcome.html)