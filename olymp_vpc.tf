
provider "aws" {
  region = "eu-central-1"
}

resource "aws_vpc" "olymp" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "olymp"
  }
}

// ---- NETWORK ---- //

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.olymp.id}"
}


resource "aws_subnet" "styx" {
  vpc_id = "${aws_vpc.olymp.id}"
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "styx"
  }
}

resource "aws_route_table" "rt" {
  vpc_id = "${aws_vpc.olymp.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }
}

resource "aws_route_table_association" "rta" {
  route_table_id = "${aws_route_table.rt.id}"
  subnet_id = "${aws_subnet.styx.id}"
}

resource "aws_security_group" "charon" {
  name = "charon"
  description = "Allow SSH inbound"
  vpc_id = "${aws_vpc.olymp.id}"

  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22

    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "charon"
  }
}

// ---- EC2 instance ---- //

resource "aws_instance" "olymp-ubuntu" {
  ami           = "ami-0bdf93799014acdc4"
  instance_type = "t2.micro"

  vpc_security_group_ids = ["${aws_security_group.charon.id}"]
  subnet_id = "${aws_subnet.styx.id}"

  availability_zone = "eu-central-1b"

  associate_public_ip_address = true

  key_name = "olymp-key"

  depends_on = ["aws_internet_gateway.gw"]

  tags = {
    Name = "olymp-ubuntu"
  }
}

resource "aws_ebs_volume" "ebs_vol" {
  availability_zone = "eu-central-1b"
  size = 1
  type = "gp2"

  tags = {
    Name = "Hades"
  }
}

resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ebs_vol.id}"
  instance_id = "${aws_instance.olymp-ubuntu.id}"
}